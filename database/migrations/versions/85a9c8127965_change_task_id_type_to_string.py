"""change task_id type to string

Revision ID: 85a9c8127965
Revises: d6b84187a3ad
Create Date: 2021-01-31 10:13:24.152302

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '85a9c8127965'
down_revision = 'd6b84187a3ad'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        table_name='ranking_tasks',
        column_name='task_id',
        type_=sa.String,
        existing_type=sa.Integer
    )


def downgrade():
    pass
