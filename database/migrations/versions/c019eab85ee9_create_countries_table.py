"""Create countries table

Revision ID: c019eab85ee9
Revises: 
Create Date: 2020-12-12 19:14:44.186086

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c019eab85ee9'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'countries',
        sa.Column('ht_league_id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('leagues', sa.JSON(), nullable=False),
    )


def downgrade():
    op.drop_table('countries')
