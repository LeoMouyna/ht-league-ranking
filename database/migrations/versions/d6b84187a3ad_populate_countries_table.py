"""populate countries table

Revision ID: d6b84187a3ad
Revises: f2487bc0969f
Create Date: 2021-01-16 08:59:44.327728

"""
from alembic import op
import sqlalchemy as sa
from os import path
from json import load

# revision identifiers, used by Alembic.
revision = 'd6b84187a3ad'
down_revision = 'f2487bc0969f'
branch_labels = None
depends_on = None

countries_table = sa.sql.table('countries',
                               sa.sql.column('ht_league_id', sa.Integer),
                               sa.sql.column('name', sa.String()),
                               sa.sql.column('leagues', sa.JSON()),
                               )


def upgrade():
    countries_file_path = path.join(path.dirname(
        path.realpath(__file__)), '../../resources/countries.json')

    with open(countries_file_path) as f:
        countries = load(f)
    inserts = [
        {'ht_league_id': c['leagueId'], 'name': c['countryName'],
            'leagues': c['leagues']}
        for c in countries
    ]
    op.bulk_insert(
        countries_table,
        inserts
    )


def downgrade():
    op.execute(
        countries_table.delete().
        where(1 == 1)
    )
