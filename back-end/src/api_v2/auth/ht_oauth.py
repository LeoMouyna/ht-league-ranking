from os import environ
from rauth import OAuth1Service, OAuth1Session
from redis import Redis
from src.api_v2.constants import REDIS_URL, HOUR, MINUTE, HT_CONSUMER_KEY, HT_CONSUMER_SECRET
from src.exceptions import APIHTTPException
from typing import Dict

SERVER_SERVICE_CONFIG = {
    "name": "server",
    "request_token_url": "https://chpp.hattrick.org/oauth/request_token.ashx",
    "access_token_url": "https://chpp.hattrick.org/oauth/access_token.ashx",
    "authorize_url": "https://chpp.hattrick.org/oauth/authorize.aspx",
    "base_url": "https://chpp.hattrick.org/chppxml.ashx",
}

CLIENT_SERVICE_CONFIG = {
    **SERVER_SERVICE_CONFIG,
    "name": "client",
    "authorize_url": "https://chpp.hattrick.org/oauth/authenticate.aspx",
}


class HTOAuth:
    def __init__(
        self,
        consumer_key: str,
        consumer_secret: str,
        access_token_key: str = "",
        access_token_secret: str = "",
        service_config: Dict = SERVER_SERVICE_CONFIG,
    ):
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.access_token_key = access_token_key
        self.access_token_secret = access_token_secret

        self.service = OAuth1Service(
            name=service_config.get("name"),
            consumer_key=self.consumer_key,
            consumer_secret=self.consumer_secret,
            request_token_url=service_config.get("request_token_url"),
            access_token_url=service_config.get("access_token_url"),
            authorize_url=service_config.get("authorize_url"),
            base_url=service_config.get("base_url"),
        )

    @property
    def session(self):
        return OAuth1Session(
            self.consumer_key,
            self.consumer_secret,
            access_token=self.access_token_key,
            access_token_secret=self.access_token_secret,
            service=self.service,
        )


class HTOAuthClient(HTOAuth):
    def __init__(self, consumer_key: str, consumer_secret: str):
        super().__init__(
            consumer_key,
            consumer_secret,
            access_token_key="",
            access_token_secret="",
            service_config=CLIENT_SERVICE_CONFIG,
        )

    def _base_request_access(self, callback_url="oob") -> str:
        (request_token, request_secret) = self.service.get_request_token(
            params={"oauth_callback": callback_url}
        )
        redis = Redis.from_url(environ[REDIS_URL])
        redis.set(request_token, request_secret)
        redis.expire(request_token, 2 * MINUTE)
        redis.close()
        return request_token

    def request_access(self, callback_url: str = "oob") -> str:
        request_token = self._base_request_access(callback_url=callback_url)
        return self.service.get_authorize_url(request_token)

    def grant_access(self, request_token_key: str, oauth_verifier: str) -> OAuth1Session:
        redis = Redis.from_url(environ[REDIS_URL])
        request_token_secret = redis.get(request_token_key)
        (access_key, access_secret) = self.service.get_access_token(
            request_token_key,
            request_token_secret,
            method="POST",
            params={"oauth_verifier": oauth_verifier},
        )
        self.access_token_key = access_key
        self.access_token_secret = access_secret
        redis.set(access_key, access_secret)
        redis.expire(access_key, 1 * HOUR)
        redis.delete(request_token_key)
        redis.close()
        return self.session


def build_client_session(access_key: str) -> OAuth1Session:
    redis = Redis.from_url(environ[REDIS_URL])
    access_secret = redis.get(access_key)
    redis.close()
    if not access_secret:
        raise APIHTTPException(
            status_code=401,
            message_code="auth.invalidToken",
            description="access key not found in redis cache",
        )

    consumer_key = environ[HT_CONSUMER_KEY]
    consumer_secret = environ[HT_CONSUMER_SECRET]
    ht_oauth = HTOAuth(
        consumer_key=consumer_key,
        consumer_secret=consumer_secret,
        access_token_key=access_key,
        access_token_secret=access_secret,
        service_config=CLIENT_SERVICE_CONFIG,
    )
    return ht_oauth.session
