from typing import Dict
from flask.json import jsonify
from flask.wrappers import Response
from src.api_v2.auth.encorder import ProfileJSONEncoder
from src.api_v2.auth.models import fetch_profile
from src.decorator import require_token
from flask import Blueprint, request
from os import environ
from flask.helpers import url_for
from werkzeug.utils import redirect
from src.api_v2.auth.ht_oauth import HTOAuthClient
from jwt import encode
from datetime import datetime
from redis import Redis
from src.api_v2.constants import (
    HT_CONSUMER_KEY,
    HT_CONSUMER_SECRET,
    JWT_TOKEN_SECRET,
    HOUR,
    REDIS_URL,
)

auth = Blueprint("auth", __name__, url_prefix="/api/v2/auth")


@auth.post("login")
def login():
    path = request.json.get("path")
    path = path if path else "/"
    consumer_key = environ[HT_CONSUMER_KEY]
    consumer_secret = environ[HT_CONSUMER_SECRET]
    ht_oauth = HTOAuthClient(consumer_key=consumer_key, consumer_secret=consumer_secret)
    url_redirection = url_for("auth.generate_token", _external=True, path=path)
    request_url = ht_oauth.request_access(url_redirection)
    res = jsonify(
        {
            "authenticated": False,
            "url": request_url,
            "message": f"Please authenticate this service at {request_url}",
        }
    )
    res.headers["Access-Control-Allow-Origin"] = "*"
    res.status_code = 201
    return res


@auth.post("logout")
@require_token
def logout(token: Dict):
    redis = Redis.from_url(environ[REDIS_URL])
    redis.delete(token.get("access_key"))
    redis.close()
    return Response(status=200)


@auth.get("generate-token")
def generate_token():
    oauth_verifier = request.args.get("oauth_verifier")
    request_key = request.args.get("oauth_token")
    path = request.args.get("path")
    consumer_key = environ[HT_CONSUMER_KEY]
    consumer_secret = environ[HT_CONSUMER_SECRET]
    ht_oauth = HTOAuthClient(consumer_key=consumer_key, consumer_secret=consumer_secret)
    session = ht_oauth.grant_access(request_key, oauth_verifier)
    now = datetime.now()
    end = now.timestamp() + 1 * HOUR
    payload = {"access_key": session.access_token, "deprecatedAt": end}
    token_secret = environ[JWT_TOKEN_SECRET]
    token = encode(payload, token_secret, algorithm="HS256")
    res = redirect(path)
    res.set_cookie("TOKEN", token, max_age=1 * HOUR, expires=end, secure=True, samesite="Lax")
    return res


@auth.get("profile")
@require_token
def profile(token: Dict):
    access_key = token.get("access_key")
    profile = fetch_profile(access_key)
    auth.json_encoder = ProfileJSONEncoder
    return jsonify(profile)
