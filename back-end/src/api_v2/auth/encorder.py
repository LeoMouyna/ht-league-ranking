from json import JSONEncoder
from src.api_v2.auth.models import Profile


class ProfileJSONEncoder(JSONEncoder):
    def default(self, o: Profile):
        return dict(name=o.name)
