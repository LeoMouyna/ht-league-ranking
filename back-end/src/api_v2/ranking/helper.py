from src.api_v2.constants import REDIS_URL
from src.api_v2 import session, logger
from src.api_v2.task.models import EnumEncoder, TaskStatusEnum
from src.exceptions import APIHTTPException
from statistics import mean, median
from xml.etree.ElementTree import Element, fromstring
from typing import List, Dict
from distutils.util import strtobool
from redis import Redis
from os import environ
from math import ceil
from json import dumps

_team_api_version = "3.5"
_league_api_version = "1.5"


class Stats:
    def __init__(
        self,
        h_mean: float = 0,
        mean: float = 0,
        h_med: float = 0,
        med: float = 0,
        maximum: float = 0,
        *args,
        **kwargs,
    ):
        super(Stats, self).__init__(*args, **kwargs)
        self.human_mean = h_mean
        self.mean = mean
        self.human_median = h_med
        self.median = med
        self.maximum = maximum


class Team:
    def __init__(self, team_id: int, is_bot: bool, power_rating: int, *args, **kwargs):
        super(Team, self).__init__(*args, **kwargs)
        self.team_id = team_id
        self.is_bot = is_bot
        self.power_rating = power_rating

    @classmethod
    def from_xml(cls, xml: Element):
        team_id = int(xml.find("TeamID").text)
        is_bot = bool(strtobool(xml.find("BotStatus").find("IsBot").text))
        power_rating = int(
            xml.find("PowerRating").find("PowerRating").text.replace("Â", "").replace("\xa0", "")
        )
        return Team(team_id=team_id, is_bot=is_bot, power_rating=power_rating)


class League:
    def __init__(
        self, league_id: int, name: str, level: int, bot_counter: int, stats: Stats, *args, **kwargs
    ):
        super(League, self).__init__(*args, **kwargs)
        self.league_id = league_id
        self.name = name
        self.level = level
        self.url = (
            f"https://www.hattrick.org/goto.ashx?path=/World/Series/?LeagueLevelUnitID={league_id}"
        )
        self.bot_counter = bot_counter
        self.stats = stats

    def __str__(self):
        return f"League {self.league_id}"

    def __repr__(self):
        return self.__str__()

    @classmethod
    def from_xml(cls, xml: Element):
        league_id = int(xml.find("LeagueLevelUnitID").text)
        name = xml.find("LeagueLevelUnitName").text
        level = int(xml.find("LeagueLevel").text)
        teams = []
        for team in xml.findall("Team"):
            team_id = int(team.find("TeamID").text)
            payload = {"file": "teamdetails", "version": _team_api_version, "teamID": team_id}
            r = session.get("", params=payload)
            if r.status_code != 200:
                logger.warning(
                    f"When fetching team {team_id}. Receive a {r.status_code} {r.reason} response"
                )
                if r.status_code == 401:
                    raise APIHTTPException(r.status_code, "errors.hattrickRejections", r.text)

                raise APIHTTPException(status_code=r.status_code, description=r.text)

            root_xml = fromstring(r.text)
            team_xml = extract_team_xml(root_xml, team_id)
            teams.append(Team.from_xml(team_xml))
        # Stats computation
        # All teams metrics
        teams_power_rating = [t.power_rating for t in teams]
        power_rating_mean = float("{:.3f}".format(mean(teams_power_rating)))
        power_rating_median = median(teams_power_rating)
        # Human teams metrics
        human_teams = [t for t in teams if not t.is_bot]
        human_teams_power_rating = [t.power_rating for t in human_teams]
        human_power_rating_median = median(human_teams_power_rating) if len(human_teams) else 0
        human_power_rating_mean = (
            float("{:.3f}".format(mean(human_teams_power_rating))) if len(human_teams) else 0
        )
        power_rating_maximum = max(human_teams_power_rating) if len(human_teams) else 0

        bot_count = len(teams) - len(human_teams)
        stats = Stats(
            h_mean=human_power_rating_mean,
            mean=power_rating_mean,
            h_med=human_power_rating_median,
            med=power_rating_median,
            maximum=power_rating_maximum,
        )

        return League(
            league_id=league_id, name=name, level=level, bot_counter=bot_count, stats=stats
        )


def extract_team_xml(xml: Element, team_id: int) -> Element:
    xpath = f".//TeamID[.='{team_id}'].."
    team_xml = xml.find(xpath)
    return team_xml


def get_league(league_id: int) -> League:
    payload = {
        "file": "leaguedetails",
        "version": _league_api_version,
        "leagueLevelUnitID": league_id,
    }
    r = session.get("", params=payload)
    if r.status_code != 200:
        logger.warning(f"Pb when fetching league {league_id}. Receive a {r.status_code} response")

        if r.status_code == 401:
            raise APIHTTPException(r.status_code, "errors.hattrickRejections", r.text)

        raise APIHTTPException(status_code=r.status_code, description=r.text)

    xml = fromstring(r.text)

    return League.from_xml(xml)


def get_leagues(first_league_id: int, league_size: int, task_id: str) -> List[Dict]:
    leagues = []

    for league_id in range(first_league_id, first_league_id + league_size):
        league = get_league(league_id=league_id)

        leagues.append(
            dict(
                leagueId=league_id,
                name=league.name,
                division=league.level,
                url=league.url,
                botCount=league.bot_counter,
                humanMean=league.stats.human_mean,
                humanMedian=league.stats.human_median,
                mean=league.stats.mean,
                median=league.stats.median,
                maximum=league.stats.maximum,
            )
        )
        if (league_id - first_league_id) % ceil(league_size / 100) == 0:
            message = dumps(
                {
                    "status": TaskStatusEnum.running,
                    "done": False,
                    "ratio": ceil((league_id - first_league_id) / league_size * 100),
                },
                cls=EnumEncoder,
            )
            try:
                redis = Redis.from_url(environ[REDIS_URL])
                redis.publish(task_id, message)
                redis.close()
            except Exception as e:
                logger.warning(f"Can't publish to {task_id} message: {message} cause:\n{e}")

    return leagues
