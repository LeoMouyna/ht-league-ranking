from datetime import datetime
from sqlalchemy.schema import Column
from enum import Enum
from sqlalchemy.types import String, Integer, DateTime
from sqlalchemy.types import Enum as DbEnum
from sqlalchemy.orm import declarative_base
from json import JSONEncoder


class TaskStatusEnum(Enum):
    pending = "pending"
    waiting = "waiting"
    done = "done"
    canceled = "canceled"
    failed = "failed"
    running = "running"


class EnumEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Enum):
            return obj.value
        return JSONEncoder.default(self, obj)


class RankingTaskModel(declarative_base()):
    """
    Ranking Task Data Model
    """

    __tablename__ = "ranking_tasks"

    task_id = Column(Integer, primary_key=True)
    task_status = Column(DbEnum(TaskStatusEnum))
    country = Column(String())
    level = Column(Integer)
    start_date = Column(DateTime, default=datetime.utcnow)

    def __init__(
        self, task_id: int, task_status: str, country: str, level: int, start_date: datetime = None
    ):
        self.task_id = task_id
        self.task_status = task_status
        self.country = country
        self.level = level
        self.start_date = start_date if start_date else datetime.utcnow()

    def __repr__(self):
        return f"<Ranking Task #{self.task_id} for {self.country} {self.level}>"
