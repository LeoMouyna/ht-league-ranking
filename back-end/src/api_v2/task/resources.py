from flask_restful import Resource
from src.api_v2.task.models import RankingTaskModel
from src.api_v2.task.schema import ranking_tasks_schema, ranking_task_schema
from src.decorator import database_connection
from sqlalchemy.orm import Session
from flask import abort


class TaskListResource(Resource):
    @database_connection
    def get(self, session: Session):
        tasks = session.query(RankingTaskModel).all()
        return ranking_tasks_schema.dump(tasks), 200


class TaskResource(Resource):
    @database_connection
    def get(self, task_id: str, session: Session):
        description = f"Task #{task_id} not found."
        task = session.get(RankingTaskModel, task_id)
        if task is None:
            abort(404, description=description)
        return ranking_task_schema.dump(task), 200
