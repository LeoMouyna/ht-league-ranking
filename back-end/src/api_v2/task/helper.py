from src.logger import set_logger
from os import environ
import logging
from redis import Redis, ConnectionError
from redis.client import PubSub
from json import loads
from src.api_v2.constants import REDIS_URL

logger = set_logger("api_v2")


def _stream_pubsub_data(pubsub: PubSub, channel: str):
    try:
        pubsub.subscribe(channel)
        logger.debug(f"Get pubsub with channels: {pubsub.channels}")
        for message in pubsub.listen():
            data = message["data"]
            logger.debug(f"Get message: {message}")
            if isinstance(data, bytes):
                data = data.decode("utf-8")
                channel = message["channel"].decode("utf-8")
                yield f"event:{channel}\ndata:{data}\n\n"

        lastData = loads(message["data"].decode("utf-8"))
        logger.debug("lastData sent: %s", lastData)
        if not lastData.get("done"):
            logging.warning("Reconnect to redis on channel %s", channel)
            return stream_task_status(channel)
    except ConnectionError as e:
        logging.error(e)
        message = "Expected redis available but got ConnectionError"
        return message, 500


def stream_task_status(task_id: str):
    try:
        redis = Redis.from_url(environ[REDIS_URL])
        pubsub = redis.pubsub()
        return _stream_pubsub_data(pubsub, task_id)

    except ConnectionError as e:
        logging.error(e)
        message = "Expected redis available but got ConnectionError"
        return message, 500
