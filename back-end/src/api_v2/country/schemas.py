from flask_marshmallow import Marshmallow

ma = Marshmallow()


class CountrySchema(ma.Schema):
    class Meta:
        fields = ("ht_league_id", "name", "leagues")


country_schema = CountrySchema()
countries_schema = CountrySchema(many=True)
