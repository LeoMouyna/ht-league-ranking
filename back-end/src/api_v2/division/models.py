from datetime import datetime
from sqlalchemy.schema import Column
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.types import String, Integer, DateTime
from sqlalchemy.orm import declarative_base


class DivisionModel(declarative_base()):
    """
    Division Data Model
    """

    __tablename__ = "divisions"

    country = Column(String(), primary_key=True)
    level = Column(Integer, primary_key=True)
    last_update = Column(DateTime, default=datetime.utcnow)
    leagues = Column(JSON)

    def __init__(self, country, level, leagues, last_update=datetime.utcnow()):
        self.country = country
        self.level = level
        self.leagues = leagues
        self.last_update = last_update

    def __repr__(self):
        return f"<Division {self.level} on {self.country}>"
