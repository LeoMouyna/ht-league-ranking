from flask_marshmallow import Marshmallow

ma = Marshmallow()


class DivisionSchema(ma.Schema):
    class Meta:
        fields = ("country", "level", "last_update", "leagues")


class DivisionSchemaLite(ma.Schema):
    class Meta:
        fields = ("country", "level", "last_update")


division_schema = DivisionSchema()
divisions_schema = DivisionSchemaLite(many=True)
