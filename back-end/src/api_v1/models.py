class League:
    def __init__(self, league_id: int):
        self.rank = None
        self.league_id = league_id
        self.name = None
        self.division = None
        self.url = f"https://www.hattrick.org/goto.ashx?path=/World/Series/\
            ?LeagueLevelUnitID={league_id}"
        self.bot_counter = 0
        self.powerStats = LeaguePowerStats()

    def __lt__(self, other):
        if self.division != other.division:
            return self.division < other.division
        else:
            return self.rank < other.rank

    def __str__(self):
        return f"League {self.league_id}"

    def __repr__(self):
        return self.__str__()


class Team:
    def __init__(self, team_id: int, is_bot: bool, power_rating: int, *args, **kwargs):
        super(Team, self).__init__(*args, **kwargs)
        self.team_id = team_id
        self.is_bot = is_bot
        self.power_rating = power_rating


class LeaguePowerStats:
    def __init__(
        self,
        h_mean: float = 0,
        mean: float = 0,
        h_med: float = 0,
        med: float = 0,
        maximum: float = 0,
        *args,
        **kwargs,
    ):
        super(LeaguePowerStats, self).__init__(*args, **kwargs)
        self.human_mean = h_mean
        self.mean = mean
        self.human_median = h_med
        self.median = med
        self.maximum = maximum
