from src.api_v1.models import League
from json import JSONEncoder


class LeagueJSONEncoder(JSONEncoder):
    def default(self, o: League):
        return dict(
            leagueId=o.league_id,
            name=o.name,
            url=o.url,
            division=o.division,
            botCount=o.bot_counter,
            humanMean=o.powerStats.human_mean,
            humanMedian=o.powerStats.human_median,
            mean=o.powerStats.mean,
            median=o.powerStats.median,
            maximum=o.powerStats.maximum,
        )
