from typing import List, Dict
from rauth import OAuth1Session
from distutils.util import strtobool
from statistics import mean, median
from src.api_v1.models import League, Team, LeaguePowerStats
import xml.etree.ElementTree as ET


def get_ints_from_string(string: str):
    return [int(s) for s in string.split() if s.isdigit()]


def stream_csv_ranks(
    leagues: List[Dict],
    fields: List[str] = [
        "humanMean",
        "humanMedian",
        "mean",
        "median",
        "maximum",
        "division",
        "botCount",
        "name",
        "url",
    ],
):
    yield ",".join(fields) + "\n"
    for league in leagues:
        elemets = []
        for field in fields:
            elemets.append(f"{league.get(field, '')}")
        yield ",".join(elemets) + "\n"


def fetch_league_data(league: League, session: OAuth1Session, api_version: str = "1.5"):
    payload = {
        "file": "leaguedetails",
        "version": api_version,
        "leagueLevelUnitID": league.league_id,
    }
    r = session.get("", params=payload)
    xml_root = ET.fromstring(r.text)
    league.name = xml_root.find("LeagueLevelUnitName").text
    league.division = int(xml_root.find("LeagueLevel").text)
    teams = [
        fetch_team_data(int(team.find("TeamID").text), session) for team in xml_root.findall("Team")
    ]
    # All teams metrics
    teams_power_rating = [t.power_rating for t in teams]
    power_rating_mean = mean(teams_power_rating)
    power_rating_median = median(teams_power_rating)
    power_rating_maximum = max(teams_power_rating)
    # Human teams metrics
    human_teams = [t for t in teams if not t.is_bot]
    human_teams_power_rating = [t.power_rating for t in human_teams]
    human_power_rating_median = median(human_teams_power_rating) if len(human_teams) else 0
    human_power_rating_mean = mean(human_teams_power_rating) if len(human_teams) else 0

    bot_count = len(teams) - len(human_teams)
    league.bot_counter = bot_count
    league.powerStats = LeaguePowerStats(
        h_mean=human_power_rating_mean,
        mean=power_rating_mean,
        h_med=human_power_rating_median,
        med=power_rating_median,
        maximum=power_rating_maximum,
    )
    return league


def fetch_team_data(team_id: int, session: OAuth1Session, api_version: str = "3.4"):
    payload = {"file": "teamdetails", "version": api_version, "teamID": team_id}
    r = session.get("", params=payload)
    xml_root = ET.fromstring(r.text)
    xpath = f".//TeamID[.='{team_id}'].."
    team = xml_root.find(xpath)
    is_bot = bool(strtobool(team.find("BotStatus").find("IsBot").text))
    power_rating = int(
        team.find("PowerRating").find("PowerRating").text.replace("Â", "").replace("\xa0", "")
    )
    return Team(team_id, is_bot, power_rating)
