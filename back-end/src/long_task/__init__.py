import logging
from os import environ
from src.api_v2.constants import REDIS_URL

from celery import Celery, Task


CELERY_TASK_LIST = ["src.long_task.ranking.tasks"]

cel_app = Celery(
    "ht-league-ranking",
    broker=environ[REDIS_URL],
    backend=environ[REDIS_URL],
    include=CELERY_TASK_LIST,
)


class LongTask(Task):
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        message = f"{task_id} failed: {exc}"
        message += f'\nReason: {kwargs["exception"]}' if kwargs.get("exception") else ""
        logging.error(message)
