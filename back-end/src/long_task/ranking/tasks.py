import logging
from typing import List, Dict

from sqlalchemy.exc import DatabaseError
from src.long_task import cel_app
from src.long_task.ranking import RankingTask
from src.api_v2.task.models import RankingTaskModel, TaskStatusEnum
from src.api_v2.ranking.helper import get_leagues
from src.decorator import database_connection
from sqlalchemy.orm import Session


@cel_app.task(name="league-compute-ranking", base=RankingTask, bind=True)
def compute_ranking(self, first_league_id: int, league_size: int) -> List[Dict]:
    """Celery task to process leagues ranking.
    It updates task status in db.
    Then process ranking.

    Args:
        first_league_id (int): First league id to compute ranking.
        league_size (int): Number of leagues to process.

    Returns:
        List[Dict]: List of metrics for asked leagues.
    """
    database_connection(update_task(self.request.id))
    return get_leagues(first_league_id, league_size, self.request.id)


@database_connection
def update_task(task_id: str, session: Session):
    current_task = None
    while not current_task:
        try:
            current_task = session.query(RankingTaskModel).get(task_id)
        except DatabaseError as err:
            logging.error(err)
    current_task.task_status = TaskStatusEnum.running
    updated_task = session.merge(current_task)
    session.add(updated_task)
    session.commit()
