from src.api_v2.auth.token import validate_token, retrieve_token
from src.api_v2.constants import JWT_TOKEN_SECRET
from os import environ
from jwt import encode
from datetime import datetime

token_secret = "qwertyuiop12345"
environ[JWT_TOKEN_SECRET] = token_secret
payload = {"access_token": "qazwsxedcrfvtgb", "deprecatedAt": datetime.now().timestamp() + 1000}


def test_valid_token():
    token = encode(payload, token_secret)
    assert validate_token(token)


def test_deprecated_token():
    payload = {"access_token": "qazwsxedcrfvtgb", "deprecatedAt": datetime.now().timestamp() - 1000}
    token = encode(payload, token_secret)
    assert not validate_token(token)


def test_invalid_token():
    token = "yjmujk,iklop657483910"
    assert not validate_token(token)


class MockRequest:
    @property
    def headers(self):
        token = encode(payload, token_secret)
        return {"Authorization": f"Bearer {token}"}


def test_retrieve_token(monkeypatch):
    request = MockRequest()
    res = retrieve_token(request)
    assert res == payload
