from src.test import RESOURCE_DIR
from os.path import join
from src.api_v1.helper import stream_csv_ranks


def test_stream_csv_ranks():
    leagues = [
        {
            "humanMean": 123,
            "humanMedian": 123,
            "mean": 12,
            "median": 12,
            "maximum": 234,
            "division": 6,
            "botCount": 4,
            "name": "VI.123",
            "url": "https://www.hattrick.org/goto.ashx?path=/World/Series/?LeagueLevelUnitID=123",
        },
        {
            "humanMean": 123,
            "humanMedian": 123,
            "mean": 12,
            "division": 6,
            "botCount": 4,
            "url": "https://www.hattrick.org/goto.ashx?path=/World/Series/?LeagueLevelUnitID=123",
        },
    ]

    csv_str = ""
    for line in stream_csv_ranks(leagues=leagues):
        csv_str += line

    assert csv_str == open(join(RESOURCE_DIR, "leagues.csv")).read()
