import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home";
import Policy from "../views/Policy";
import About from "../views/About";
import Results from "../views/Results";

Vue.use(VueRouter);

const routes = [
  { path: "/", name: "Home", component: Home },
  { path: "/policy", name: "Policy", component: Policy },
  { path: "/about", name: "About", component: About },
  {
    path: "/results",
    name: "Results",
    component: Results,
    props: route => ({
      countryName: route.query.country,
      divisionLevel: Number(route.query.level)
    })
  },
  { path: "/*", redirect: { name: "Home" } }
];

const router = new VueRouter({
  mode: "history",
  routes
});

export default router;
