import Cookies from "js-cookie";
import { API_v1, API_v2 } from "./httpService";

const COOKIE_TOKEN_KEY = "TOKEN";
const STORAGE_TOKEN_KEY = "token";

export const authStatus = () => {
  return API_v1.get("auth/status");
};

export const login = () => {
  return API_v2.post("auth/login", { path: window.location.href });
};

export const logout = () => {
  const headers = buildAuthorizationHeader();
  return API_v2.post("auth/logout", undefined, { headers });
};

export const fetchProfile = () => {
  const headers = buildAuthorizationHeader();
  return API_v2.get("auth/profile", { headers });
};

export const findTokenInCookies = () => {
  return Cookies.get(COOKIE_TOKEN_KEY);
};

export const storeToken = token => {
  return localStorage.setItem(STORAGE_TOKEN_KEY, token);
};

export const retrieveToken = () => {
  return localStorage.getItem(STORAGE_TOKEN_KEY);
};

export const removeToken = () => {
  Cookies.remove(COOKIE_TOKEN_KEY);
  localStorage.removeItem(STORAGE_TOKEN_KEY);
};

export const buildAuthorizationHeader = () => {
  const token = retrieveToken();
  return token ? { Authorization: `Bearer ${token}` } : {};
};

export default {
  authStatus,
  login,
  logout,
  findTokenInCookies,
  storeToken,
  retrieveToken,
  removeToken,
  buildAuthorizationHeader,
  fetchProfile
};
