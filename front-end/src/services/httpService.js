import axios from "axios";

export const API_v1 = axios.create({
  baseURL: "api/v1",
  // baseURL: "http://localhost:5000/api/v1",
  crossdomain: true
});

export const API_v2 = axios.create({
  baseURL: "api/v2",
  // baseURL: "http://localhost:5000/api/v2",
  crossdomain: true
});

export const STREAM_API_V2_URL = "api/v2/stream";
// export const STREAM_API_V2_URL = "http://localhost:5000/api/v2/stream";
