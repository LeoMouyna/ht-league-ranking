import { API_v2 } from "./httpService";

const getOne = countryId => {
  return API_v2.get(`countries/${countryId}`);
};

const getAll = () => {
  return API_v2.get("countries");
};

const getByName = countryName => {
  return API_v2.get(`countries?name=${countryName}`);
};

export default {
  getOne,
  getAll,
  getByName
};
