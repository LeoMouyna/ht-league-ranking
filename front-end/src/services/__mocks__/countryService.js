const countries = [
  {
    ht_league_id: 1,
    name: "Sweden",
    leagues: [
      { level: 1, firstId: 1, size: 1 },
      { level: 2, firstId: 2, size: 4 }
    ]
  },
  {
    ht_league_id: 2,
    name: "France",
    leagues: [
      { level: 1, firstId: 6, size: 1 },
      { level: 2, firstId: 7, size: 4 }
    ]
  },
  {
    ht_league_id: 3,
    name: "Finland",
    leagues: [
      { level: 1, firstId: 11, size: 1 },
      { level: 2, firstId: 12, size: 4 },
      { level: 3, firstId: 16, size: 16 }
    ]
  }
];

const getAll = jest
  .fn(() => {
    return Promise.resolve({
      data: countries
    });
  })
  .mockName("Default data");

const getByName = jest.fn(name => {
  const filtered = countries.filter(country =>
    country.name.toLowerCase().includes(name.toLowerCase())
  );
  return Promise.resolve({
    data: filtered
  });
});

export default {
  getAll,
  getByName,
  countries
};
