class API {
  get = jest.fn();
  post = jest.fn();
  put = jest.fn();
  delete = jest.fn();
}

export const API_v1 = new API();

export const API_v2 = new API();

export const STREAM_API_V2_URL = "test-api/v2/stream";
