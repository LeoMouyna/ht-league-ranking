const getOne = jest.fn(leagueId => {
  return Promise.resolve({
    data: {
      botCount: 1,
      division: 3,
      humanMean: 977.857,
      humanMedian: 975,
      leagueId,
      maximum: 1044,
      mean: 971.375,
      median: 972.0,
      name: "III.15",
      url:
        "https://www.hattrick.org/goto.ashx?path=/World/Series/            ?LeagueLevelUnitID=701"
    }
  });
});

const getDivision = jest.fn((country, level) => {
  return Promise.resolve({
    data: {
      leagues: [
        {
          leagueId: 513,
          name: "II.1",
          division: level,
          url:
            "https://www.hattrick.org/goto.ashx?path=/World/Series/?LeagueLevelUnitID=513",
          botCount: 0,
          humanMean: 1074.125,
          humanMedian: 1082.5,
          mean: 1074.125,
          median: 1082.5,
          maximum: 1131
        },
        {
          leagueId: 514,
          name: "II.2",
          division: level,
          url:
            "https://www.hattrick.org/goto.ashx?path=/World/Series/?LeagueLevelUnitID=514",
          botCount: 0,
          humanMean: 1104.375,
          humanMedian: 1108.5,
          mean: 1104.375,
          median: 1108.5,
          maximum: 1165
        },
        {
          leagueId: 515,
          name: "II.3",
          division: level,
          url:
            "https://www.hattrick.org/goto.ashx?path=/World/Series/?LeagueLevelUnitID=515",
          botCount: 0,
          humanMean: 1047.125,
          humanMedian: 1065.5,
          mean: 1047.125,
          median: 1065.5,
          maximum: 1130
        },
        {
          leagueId: 516,
          name: "II.4",
          division: level,
          url:
            "https://www.hattrick.org/goto.ashx?path=/World/Series/?LeagueLevelUnitID=516",
          botCount: 0,
          humanMean: 1079.375,
          humanMedian: 1082.0,
          mean: 1079.375,
          median: 1082.0,
          maximum: 1138
        }
      ],
      country,
      last_update: new Date().toISOString(),
      level
    }
  });
});

const forceDivision = jest.fn((country, level, firstLeagueId, leagueSize) => {
  return Promise.resolve({
    data: {
      task_id: "92c5b973-0dfc-4227-a28e-82bba8e19cd7",
      task_status: "pending",
      country,
      level,
      start_date: new Date().toISOString()
    }
  });
});

const getTask = jest.fn(taskId => {
  return Promise.resolve({
    data: {
      task_id: taskId,
      task_status: "pending",
      country: "Test",
      level: 0,
      start_date: new Date().toISOString()
    }
  });
});

const getTaskEvent = jest.fn(taskId => {
  return {
    addEventListener: jest.fn(),
    close: jest.fn
  };
});

const download = jest.fn(leagues => {
  return Promise.resolve({
    data:
      "aHVtYW5NZWFuLGh1bWFuTWVkaWFuLG1lYW4sbWVkaWFuLG1heGltdW0sZGl2aXNpb24sYm90Q291bnQsbmFtZSx1cmwKMTA2NC43NSwxMDc4LDEwNjQuNzUsMTA3OCwxMTI3LDIsMCxJSS4xLGh0dHBzOi8vd3d3LmhhdHRyaWNrLm9yZy9nb3RvLmFzaHg/cGF0aD0vV29ybGQvU2VyaWVzLz9MZWFndWVMZXZlbFVuaXRJRD01MTMKMTAzOC4zNzUsMTA5Mi41LDEwMzguMzc1LDEwOTIuNSwxMTU4LDIsMCxJSS4yLGh0dHBzOi8vd3d3LmhhdHRyaWNrLm9yZy9nb3RvLmFzaHg/cGF0aD0vV29ybGQvU2VyaWVzLz9MZWFndWVMZXZlbFVuaXRJRD01MTQKMTA0My4xMjUsMTA5MCwxMDQzLjEyNSwxMDkwLDExMzIsMiwwLElJLjMsaHR0cHM6Ly93d3cuaGF0dHJpY2sub3JnL2dvdG8uYXNoeD9wYXRoPS9Xb3JsZC9TZXJpZXMvP0xlYWd1ZUxldmVsVW5pdElEPTUxNQoxMDc3LjI1LDEwODIuNSwxMDc3LjI1LDEwODIuNSwxMTIwLDIsMCxJSS40LGh0dHBzOi8vd3d3LmhhdHRyaWNrLm9yZy9nb3RvLmFzaHg/cGF0aD0vV29ybGQvU2VyaWVzLz9MZWFndWVMZXZlbFVuaXRJRD01MTYK"
  });
});

export default {
  getOne,
  getDivision,
  forceDivision,
  getTask,
  getTaskEvent,
  download
};
