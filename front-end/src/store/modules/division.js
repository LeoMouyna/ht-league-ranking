import helper from "./helper";

const state = () => ({
  country: undefined,
  countryId: undefined,
  level: undefined,
  firstLeagueId: undefined,
  leagueSize: undefined
});

const actions = {
  setDivision(
    { commit },
    { level, firstLeagueId, leagueSize, country, countryId }
  ) {
    commit("setLevel", level);
    commit("setFirstLeagueId", firstLeagueId);
    commit("setLeagueSize", leagueSize);
    commit("setCountry", country);
    commit("setCountryId", countryId);
  }
};

const mutations = {
  setCountry(state, country) {
    helper.checkFunctionArgumentType("setCountry", country, "string");
    state.country = country;
  },
  setCountryId(state, countryId) {
    helper.checkFunctionArgumentType("setCountryId", countryId, "number");
    state.countryId = countryId;
  },
  setLevel(state, level) {
    helper.checkFunctionArgumentType("setLevel", level, "number");
    state.level = level;
  },
  setFirstLeagueId(state, firstLeagueId) {
    helper.checkFunctionArgumentType(
      "setFirstLeagueId",
      firstLeagueId,
      "number"
    );
    state.firstLeagueId = firstLeagueId;
  },
  setLeagueSize(state, leagueSize) {
    helper.checkFunctionArgumentType("setLeagueSize", leagueSize, "number");
    state.leagueSize = leagueSize;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
