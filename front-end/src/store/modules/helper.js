const checkFunctionArgumentType = (
  functionName,
  argumentValue,
  argumentType
) => {
  if (typeof argumentValue !== argumentType) {
    throw new TypeError(
      `${functionName} received: ${argumentValue} which is not ${argumentType}.`
    );
  }
};

export default {
  checkFunctionArgumentType
};
