import { mapState, mapActions, mapGetters } from "vuex";
import authService from "../services/authService";

const handleUnauthorized = async (
  req,
  unauthorizedCallBack = async () => {}
) => {
  try {
    return await req();
  } catch (error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      if (error.response.status === 401) {
        await unauthorizedCallBack();
      }
      return Promise.reject(error.response.data);
    } else {
      return Promise.reject(error.message);
    }
  }
};

const sharedMethods = {
  ...mapActions("authentication", {
    updateStoreToken: "updateToken",
    updateStoreProfile: "updateProfile",
    storeLogout: "logout"
  }),
  async syncUserData() {
    try {
      const res = await handleUnauthorized(
        authService.fetchProfile,
        this.removeAuthToken
      );
      const profile = res.data;
      this.updateStoreProfile(profile);
    } catch (e) {
      const message = e.code ?? "errors.somethingWentWrong";
      const type = e.code ? "is-warning" : "is-danger";
      this.$buefy.toast.open({
        message: this.$t(message),
        type
      });
    }
  },
  removeAuthToken() {
    this.storeLogout();
    authService.removeToken();
  }
};

const sharedComputed = {
  ...mapGetters("authentication", ["userAuthenticated"]),
  ...mapState("authentication", ["profile"])
};

export const authViewMixin = {
  computed: {
    ...sharedComputed
  },
  methods: {
    ...sharedMethods
  },
  async beforeMount() {
    if (this.userAuthenticated) {
      if (!this.profile) {
        return await this.syncUserData();
      }
    } else {
      const storageToken = authService.retrieveToken();
      const cookieToken = authService.findTokenInCookies();
      if (storageToken) {
        this.updateStoreToken(storageToken);
        return await this.syncUserData();
      } else if (cookieToken) {
        authService.storeToken(cookieToken);
        this.updateStoreToken(cookieToken);
        return await this.syncUserData();
      }
    }
  }
};

export const authComponentMixin = {
  computed: {
    ...sharedComputed
  },
  methods: {
    ...sharedMethods,
    async login() {
      try {
        const res = await authService.login();
        const { url } = res.data;
        this.$buefy.toast.open({
          message: `${this.$t(
            "links.redirection"
          )} <a href=${url}>hattrick</a>`,
          type: "is-warning"
        });
        setTimeout(() => {
          window.location = url;
        }, 3000);
      } catch (e) {
        this.$buefy.toast.open({
          message: this.$t("errors.somethingWentWrong", { e }),
          type: "is-danger"
        });
      }
    },
    async logout() {
      try {
        await handleUnauthorized(authService.logout);
      } catch (e) {
        console.error(e);
        this.$buefy.toast.open({
          message: this.$t("errors.somethingWentWrong", { e }),
          type: "is-danger"
        });
      } finally {
        this.removeAuthToken();
      }
    }
  }
};
