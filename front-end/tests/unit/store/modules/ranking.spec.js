import { createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import ranking from "@/store/modules/ranking";

const localVue = createLocalVue();
localVue.use(Vuex);
let store;

describe("ranking store module", () => {
  beforeEach(() => {
    store = new Vuex.Store({ ...ranking });
  });
  test("Initial setup", () => {
    expect(store.state.leagues).toMatchObject([]);
    expect(store.state.last_update).toBeUndefined();
    expect(store.state.country).toBeUndefined();
    expect(store.state.level).toBeUndefined();
    expect(store.state.loading).toBeFalsy();
  });
  describe("mutations", () => {
    describe("setLeagues", () => {
      test("set to a list", () => {
        const leagues = [
          { country: "Sweden", leagueId: 1 },
          { country: "Sweden", leagueId: 2 }
        ];
        store.commit("setLeagues", leagues);
        expect(store.state.leagues).toMatchObject(leagues);
      });
    });
    describe("setUpdate", () => {
      test("set to a date string", () => {
        const now = new Date().toISOString();
        store.commit("setUpdate", now);
        expect(store.state.last_update).toBe(now);
      });
    });
    describe("setCountry", () => {
      test("set to a string", () => {
        store.commit("setCountry", "Sweden");
        expect(store.state.country).toBe("Sweden");
      });
    });
    describe("setLevel", () => {
      test("set to a number", () => {
        store.commit("setLevel", 5);
        expect(store.state.level).toBe(5);
      });
    });
    describe("toggleLoading", () => {
      test("toogle to opposite value", () => {
        expect(store.state.loading).toBeFalsy();
        store.commit("toggleLoading");
        expect(store.state.loading).toBeTruthy();
        store.commit("toggleLoading");
        expect(store.state.loading).toBeFalsy();
      });
      test("toogle from true to false", () => {
        store.state.loading = true;
        expect(store.state.loading).toBeTruthy();
        store.commit("toggleLoading");
        expect(store.state.loading).toBeFalsy();
      });
    });
    describe("addLeague", () => {
      test("set to a list", () => {
        const leagues = [
          { country: "Sweden", leagueId: 1 },
          { country: "Sweden", leagueId: 2 }
        ];
        store.state.leagues = leagues;
        expect(store.state.leagues.length).toBe(2);
        store.commit("addLeague", { country: "Sweden", leagueId: 3 });
        expect(store.state.leagues.length).toBe(3);
        expect(store.state.leagues[2]).toMatchObject({
          country: "Sweden",
          leagueId: 3
        });
      });
    });
  });
  describe("actions", () => {
    const commit = jest.fn();
    beforeEach(() => {
      commit.mockClear();
    });
    describe("set", () => {
      test("use commit to needed state values", () => {
        const leagues = [
          { country: "Sweden", leagueId: 1 },
          { country: "Sweden", leagueId: 2 },
          { country: "Sweden", leagueId: 3 }
        ];
        const last_update = new Date().toISOString();
        const country = "Whatever";
        const level = 3;
        ranking.actions.set(
          { commit },
          { leagues, last_update, country, level }
        );
        expect(commit).toHaveBeenCalledWith("setLeagues", leagues);
        expect(commit).toHaveBeenCalledWith("setUpdate", last_update);
        expect(commit).toHaveBeenCalledWith("setLevel", level);
        expect(commit).toHaveBeenCalledWith("setCountry", country);
      });
      describe("when wrong params are passed", () => {
        let leagues;
        let last_update;
        let country;
        let level;
        beforeEach(() => {
          leagues = [
            { country: "Sweden", leagueId: 1 },
            { country: "Sweden", leagueId: 2 },
            { country: "Sweden", leagueId: 3 }
          ];
          last_update = new Date().toISOString();
          country = "Whatever";
          level = 3;
        });
        describe("leagues", () => {
          describe("set as undefined", () => {
            beforeEach(() => {
              leagues = undefined;
            });
            test("throw type error", () => {
              expect(() => {
                ranking.actions.set(
                  { commit },
                  { leagues, last_update, country, level }
                );
              }).toThrowError(TypeError);
            });
          });
          describe("set as a number", () => {
            beforeEach(() => {
              leagues = 123;
            });
            test("throw type error", () => {
              expect(() => {
                ranking.actions.set(
                  { commit },
                  { leagues, last_update, country, level }
                );
              }).toThrowError(TypeError);
            });
          });
          describe("set as a string", () => {
            beforeEach(() => {
              leagues = "123";
            });
            test("throw type error", () => {
              expect(() => {
                ranking.actions.set(
                  { commit },
                  { leagues, last_update, country, level }
                );
              }).toThrowError(TypeError);
            });
          });
        });
        describe("last_update", () => {
          describe("set as undefined", () => {
            beforeEach(() => {
              last_update = undefined;
            });
            test("throw type error", () => {
              expect(() => {
                ranking.actions.set(
                  { commit },
                  { leagues, last_update, country, level }
                );
              }).toThrowError(TypeError);
            });
          });
          describe("set as Date object", () => {
            beforeEach(() => {
              last_update = new Date();
            });
            test("throw type error", () => {
              expect(() => {
                ranking.actions.set(
                  { commit },
                  { leagues, last_update, country, level }
                );
              }).toThrowError(TypeError);
            });
          });
        });
        describe("country", () => {
          describe("set as undefined", () => {
            beforeEach(() => {
              country = undefined;
            });
            test("throw type error", () => {
              expect(() => {
                ranking.actions.set(
                  { commit },
                  { leagues, last_update, country, level }
                );
              }).toThrowError(TypeError);
            });
          });
          describe("set as a number", () => {
            beforeEach(() => {
              country = 1234;
            });
            test("throw type error", () => {
              expect(() => {
                ranking.actions.set(
                  { commit },
                  { leagues, last_update, country, level }
                );
              }).toThrowError(TypeError);
            });
          });
        });
        describe("level", () => {
          describe("set as undefined", () => {
            beforeEach(() => {
              level = undefined;
            });
            test("throw type error", () => {
              expect(() => {
                ranking.actions.set(
                  { commit },
                  { leagues, last_update, country, level }
                );
              }).toThrowError(TypeError);
            });
          });
          describe("set as a string", () => {
            beforeEach(() => {
              level = "qwertyu";
            });
            test("throw type error", () => {
              expect(() => {
                ranking.actions.set(
                  { commit },
                  { leagues, last_update, country, level }
                );
              }).toThrowError(TypeError);
            });
          });
        });
      });
    });
    describe("add", () => {
      test("use commit to needed state values", () => {
        const league = { country: "Sweden", leagueId: 4 };

        ranking.actions.add({ commit }, league);
        expect(commit).toHaveBeenCalledWith("addLeague", league);
      });
    });
    describe("toggleLoading", () => {
      test("use commit to toogle loading state value", () => {
        ranking.actions.toggleLoading({ commit });
        expect(commit).toHaveBeenCalledWith("toggleLoading");
      });
    });
    describe("reset", () => {
      test("use commit to reset leagues", () => {
        ranking.actions.reset({ commit });
        expect(commit).toHaveBeenCalledWith("setLeagues", []);
      });
      test("use commit to reset last_update date", () => {
        ranking.actions.reset({ commit });
        expect(commit).toHaveBeenCalledWith("setUpdate", undefined);
      });
      test("use commit to reset country", () => {
        ranking.actions.reset({ commit });
        expect(commit).toHaveBeenCalledWith("setCountry", undefined);
      });
      test("use commit to reset level", () => {
        ranking.actions.reset({ commit });
        expect(commit).toHaveBeenCalledWith("setLevel", undefined);
      });
    });
  });
});
