import { createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import task from "@/store/modules/task";

const localVue = createLocalVue();
localVue.use(Vuex);
let store;

describe("task store module", () => {
  beforeEach(() => {
    store = new Vuex.Store({ ...task });
  });
  test("Initial setup", () => {
    expect(store.state.taskId).toBeUndefined();
    expect(store.state.taskStatus).toBe("done");
    expect(store.state.country).toBeUndefined();
    expect(store.state.level).toBeUndefined();
    expect(store.state.startDate).toBeUndefined();
    expect(store.state.ratio).toBeUndefined();
  });
  describe("mutations", () => {
    describe("setCountry", () => {
      test("set to 'France'", () => {
        store.commit("setCountry", "France");
        expect(store.state.country).toBe("France");
      });
      test("set to not string value", () => {
        expect(() => {
          return store.commit("setCountry", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setCountry", 1234);
        }).toThrowError(TypeError);
      });
    });
    describe("resetCountry", () => {
      test("set to undefined", () => {
        store.commit("resetCountry");
        expect(store.state.country).toBeUndefined();
      });
    });
    describe("setTaskId", () => {
      test("set to a string", () => {
        store.commit("setTaskId", "6e8a5034-2e09-4eeb-aedd-7748c3da1ffa");
        expect(store.state.taskId).toBe("6e8a5034-2e09-4eeb-aedd-7748c3da1ffa");
      });
      test("set to not string value", () => {
        expect(() => {
          return store.commit("setTaskId", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setTaskId", 1234);
        }).toThrowError(TypeError);
      });
    });
    describe("resetTaskId", () => {
      test("set to undefined", () => {
        store.commit("resetTaskId");
        expect(store.state.taskId).toBeUndefined();
      });
    });
    describe("setLevel", () => {
      test("set to a number", () => {
        store.commit("setLevel", 1234);
        expect(store.state.level).toBe(1234);
      });
      test("set to not number value", () => {
        expect(() => {
          return store.commit("setLevel", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setLevel", "hello");
        }).toThrowError(TypeError);
      });
    });
    describe("resetLevel", () => {
      test("set to undefined", () => {
        store.commit("resetLevel");
        expect(store.state.level).toBeUndefined();
      });
    });
    describe("setTaskStatus", () => {
      test("set to a string", () => {
        store.commit("setTaskStatus", "WIP");
        expect(store.state.taskStatus).toBe("WIP");
      });
      test("set to not string value", () => {
        expect(() => {
          return store.commit("setTaskStatus", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setTaskStatus", 1234);
        }).toThrowError(TypeError);
      });
    });
    describe("resetTaskStatus", () => {
      test("set to 'done'", () => {
        store.commit("resetTaskStatus");
        expect(store.state.taskStatus).toBe("done");
      });
    });
    describe("setStartDate", () => {
      test("set to a string date", () => {
        const now = new Date().toISOString();
        store.commit("setStartDate", now);
        expect(store.state.startDate).toBe(now);
      });
      test("set to not string date value", () => {
        expect(() => {
          return store.commit("setStartDate", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setStartDate", 1234);
        }).toThrowError(TypeError);
      });
    });
    describe("resetStartDate", () => {
      test("set to undefined", () => {
        store.commit("resetStartDate");
        expect(store.state.startDate).toBeUndefined();
      });
    });
    describe("setRatio", () => {
      test("set to a number", () => {
        store.commit("setRatio", 45);
        expect(store.state.ratio).toBe(45);
      });
      test("set to not number value", () => {
        expect(() => {
          return store.commit("setRatio", undefined);
        }).toThrowError(TypeError);
        expect(() => {
          return store.commit("setRatio", "hello");
        }).toThrowError(TypeError);
      });
    });
    describe("resetRatio", () => {
      test("set to undefined", () => {
        store.commit("resetRatio");
        expect(store.state.ratio).toBeUndefined();
      });
    });
  });
  describe("actions", () => {
    const commit = jest.fn();
    beforeEach(() => {
      commit.mockClear();
    });
    describe("setTask", () => {
      test("use commit to needed state values", () => {
        const t = {
          country: "France",
          start_date: "2021-04-04T14:10:55.418917",
          task_id: "6e8a5034-2e09-4eeb-aedd-7748c3da1ffa",
          task_status: "pending",
          level: 2
        };
        task.actions.setTask({ commit }, t);
        expect(commit).toHaveBeenCalledTimes(6);
        expect(commit).toHaveBeenCalledWith("setLevel", t.level);
        expect(commit).toHaveBeenCalledWith("setTaskId", t.task_id);
        expect(commit).toHaveBeenCalledWith("setTaskStatus", t.task_status);
        expect(commit).toHaveBeenCalledWith("setCountry", t.country);
        expect(commit).toHaveBeenCalledWith("setStartDate", t.start_date);
        expect(commit).toHaveBeenCalledWith("resetRatio");
      });
    });
    describe("updateRunningTask", () => {
      test("use commit to needed state values", () => {
        const t = {
          status: "running",
          ratio: 56
        };
        task.actions.updateRunningTask({ commit }, t);
        expect(commit).toHaveBeenCalledTimes(2);
        expect(commit).toHaveBeenCalledWith("setTaskStatus", t.status);
        expect(commit).toHaveBeenCalledWith("setRatio", t.ratio);
      });
    });
    describe("resetTask", () => {
      test("use commit to needed state values", () => {
        task.actions.resetTask({ commit });
        expect(commit).toHaveBeenCalledTimes(6);
        expect(commit).toHaveBeenCalledWith("resetLevel");
        expect(commit).toHaveBeenCalledWith("resetTaskId");
        expect(commit).toHaveBeenCalledWith("resetTaskStatus");
        expect(commit).toHaveBeenCalledWith("resetCountry");
        expect(commit).toHaveBeenCalledWith("resetStartDate");
        expect(commit).toHaveBeenCalledWith("resetRatio");
      });
    });
  });
  describe("getters", () => {
    describe("complete", () => {
      test("return true for 'done' status", () => {
        store.state.taskStatus = "done";
        expect(task.getters.complete(store.state)).toBeTruthy();
      });
      test("return true for 'failed' status", () => {
        store.state.taskStatus = "failed";
        expect(task.getters.complete(store.state)).toBeTruthy();
      });
      test("return true for 'canceled' status", () => {
        store.state.taskStatus = "canceled";
        expect(task.getters.complete(store.state)).toBeTruthy();
      });
      test("return false for other status", () => {
        store.state.taskStatus = "pending";
        expect(task.getters.complete(store.state)).toBeFalsy();
        store.state.taskStatus = undefined;
        expect(task.getters.complete(store.state)).toBeFalsy();
      });
    });
    describe("done", () => {
      test("return true for 'done' status", () => {
        store.state.taskStatus = "done";
        expect(task.getters.done(store.state)).toBeTruthy();
      });
      test("return false for other status", () => {
        store.state.taskStatus = "canceled";
        expect(task.getters.done(store.state)).toBeFalsy();
        store.state.taskStatus = "failed";
        expect(task.getters.done(store.state)).toBeFalsy();
        store.state.taskStatus = "pending";
        expect(task.getters.done(store.state)).toBeFalsy();
        store.state.taskStatus = undefined;
        expect(task.getters.done(store.state)).toBeFalsy();
      });
    });
  });
});
