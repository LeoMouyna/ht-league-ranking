import countryService from "@/services/countryService";
import { API_v2, API_v1 } from "@/services/httpService";

jest.mock("@/services/httpService");

describe("countryService", () => {
  beforeEach(() => {
    API_v2.get.mockClear();
  });
  describe("getOne", () => {
    test("generate a get request", () => {
      countryService.getOne(1234);
      expect(API_v2.get).toHaveBeenCalledTimes(1);
    });
    test("use given countryId on get url", () => {
      countryService.getOne(120);
      expect(API_v2.get).toHaveBeenCalledWith("countries/120");

      for (var i = 123; i < 130; i++) {
        const countryId = i;
        countryService.getOne(countryId);
        expect(API_v2.get).toHaveBeenCalledWith(`countries/${countryId}`);
      }
    });
  });
  describe("getAll", () => {
    test("http call to countries", () => {
      countryService.getAll();
      expect(API_v2.get).toHaveBeenCalledTimes(1);
      expect(API_v2.get).toHaveBeenCalledWith("countries");
    });
  });
  describe("getByName", () => {
    test("generate a get request", () => {
      countryService.getByName(1234);
      expect(API_v2.get).toHaveBeenCalledTimes(1);
    });
    test("use given countryName on get url", () => {
      countryService.getByName("hello");
      expect(API_v2.get).toHaveBeenCalledWith("countries?name=hello");

      ["fra", "sw", "EnGlanD", "Eire"].every(country => {
        countryService.getByName(country);
        expect(API_v2.get).toHaveBeenCalledWith(`countries?name=${country}`);
      });
    });
  });
  afterEach(() => {
    expect(API_v1.get).toHaveBeenCalledTimes(0);
  });
});
