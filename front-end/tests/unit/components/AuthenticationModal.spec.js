import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import AuthenticationModal from "@/components/AuthenticationModal.vue";
import Buefy from "buefy";
import Vuex from "vuex";
import authService from "@/services/authService.js";

jest.mock("@/services/authService.js");

config.mocks.$t = key => key;

const localVue = createLocalVue();
localVue.use(Buefy);
localVue.use(Vuex);

const $buefy = {
  toast: {
    open: jest.fn()
  }
};

const actions = {
  set: jest.fn()
};

const store = new Vuex.Store({
  modules: {
    authentication: {
      namespaced: true,
      actions
    }
  }
});

let wrapper = shallowMount(AuthenticationModal, {
  localVue,
  store
});

describe("AuthenticationModal component", () => {
  describe("#mounted", () => {
    describe("When authService return server already authenticated message", () => {
      test("Set authChecked", async () => {
        expect(authService.authStatus).toHaveBeenCalled();
        expect(wrapper.vm.$data.authChecked).toBeTruthy();
        expect(wrapper.vm.$data.url).toBe("");
      });
      test("Call authentication set from store", async () => {
        expect(authService.authStatus).toHaveBeenCalled();
        expect(actions.set).toHaveBeenCalledWith(expect.any(Object), true);
      });
    });
    describe("When authService return authentication needed message", () => {
      beforeAll(() => {
        authService.authStatus.mockImplementation(() => {
          return Promise.resolve({
            data: {
              authenticated: false,
              url: "https://test.com",
              message: "Please authenticate this service at https://test.com"
            }
          });
        });
        wrapper = shallowMount(AuthenticationModal, {
          localVue,
          store
        });
      });
      test("Set authChecked and url", async () => {
        expect(authService.authStatus).toHaveBeenCalled();
        expect(wrapper.vm.$data.authChecked).toBeTruthy();
        expect(wrapper.vm.$data.url).toBe("https://test.com");
      });
      test("Call authentication set from store", async () => {
        expect(authService.authStatus).toHaveBeenCalled();
        expect(actions.set).toHaveBeenCalledWith(expect.any(Object), false);
      });
    });
    describe("When authService return an error", () => {
      beforeAll(() => {
        authService.authStatus.mockImplementation(() => {
          return Promise.reject("test rejection");
        });
        wrapper = shallowMount(AuthenticationModal, {
          localVue,
          mocks: {
            $buefy
          }
        });
      });
      test("Handle promise reject", () => {
        expect(authService.authStatus).toHaveBeenCalled();
        expect(wrapper.vm.$data.authChecked).toBeTruthy();
        expect(wrapper.vm.$buefy.toast.open).toHaveBeenCalledWith({
          message: "errors.somethingWentWrong",
          type: "is-danger"
        });
      });
    });
  });
  describe("#fetchNeeded", () => {
    beforeEach(() => {
      wrapper = shallowMount(AuthenticationModal, {
        localVue,
        store
      });
    });
    test("Follow authChecked value", () => {
      wrapper.vm.$data.authChecked = false;
      expect(wrapper.vm.fetchNeeded).toBeTruthy();
      wrapper.vm.$data.authChecked = undefined;
      expect(wrapper.vm.fetchNeeded).toBeTruthy();
      wrapper.vm.$data.authChecked = null;
      expect(wrapper.vm.fetchNeeded).toBeTruthy();
      wrapper.vm.$data.authChecked = true;
      expect(wrapper.vm.fetchNeeded).toBeFalsy();
    });
  });
  describe("#authNeeded", () => {
    beforeEach(() => {
      wrapper = shallowMount(AuthenticationModal, {
        localVue,
        store
      });
    });
    test("With initial data", () => {
      wrapper.vm.$data.authChecked = false;
      wrapper.vm.$data.url = "";
      expect(wrapper.vm.authNeeded).toBeFalsy();
    });
    describe("With fixed authChecked to true", () => {
      beforeEach(() => {
        wrapper.vm.$data.authChecked = true;
      });
      test("Follow url value", () => {
        wrapper.vm.$data.url = "";
        expect(wrapper.vm.authNeeded).toBeFalsy();
        wrapper.vm.$data.url = undefined;
        expect(wrapper.vm.authNeeded).toBeFalsy();
        wrapper.vm.$data.url = null;
        expect(wrapper.vm.authNeeded).toBeFalsy();
        wrapper.vm.$data.url = "https://test.com";
        expect(wrapper.vm.authNeeded).toBeTruthy();
      });
    });
  });
});
