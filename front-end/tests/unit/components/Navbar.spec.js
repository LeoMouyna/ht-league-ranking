import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import Navbar from "@/components/Navbar.vue";
import Vuex from "vuex";
import Buefy from "buefy";
import VueI18n from "vue-i18n";
import { buildStore } from "../helpers/store.js";

config.mocks.$t = key => key;

const localVue = createLocalVue();
localVue.use(Buefy);
localVue.use(VueI18n);
localVue.use(Vuex);

const i18n = new VueI18n({
  local: "en-us"
});

let wrapper;
const store = buildStore(null, null, null, null, null, null, null, null);
describe("Navbar.vue", () => {
  beforeAll(() => {
    wrapper = shallowMount(Navbar, {
      localVue,
      i18n,
      store
    });
  });
  test("mount correctly", () => {
    expect(wrapper).toBeTruthy();
  });
});
