import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import RankingTable from "@/components/RankingTable.vue";
import deviceService from "@/services/deviceService.js";
import Buefy from "buefy";
import Vuex from "vuex";
import ranking from "@/store/modules/ranking";
import rankingService from "@/services/rankingService.js";

config.mocks.$t = key => key;

jest.mock("@/services/deviceService.js");
jest.mock("@/services/rankingService.js");

const localVue = createLocalVue();
localVue.use(Buefy);
localVue.use(Vuex);

const $buefy = {
  toast: {
    open: jest.fn()
  }
};

let store;
let wrapper;

describe("RankingTable component", () => {
  describe("Device tweak", () => {
    beforeEach(() => {
      const state = () => ({
        leagues: [],
        last_update: undefined,
        country: undefined,
        level: undefined,
        loading: false
      });
      store = new Vuex.Store({
        modules: {
          ranking: {
            namespaced: true,
            state,
            getters: ranking.getters
          }
        }
      });
      wrapper = shallowMount(RankingTable, {
        localVue,
        store
      });
    });
    describe("On desktop device", () => {
      test("pagination is set to 20", () => {
        expect(wrapper.vm.$data.pagination).toBe(20);
      });
      test("size is set to is-large", () => {
        expect(wrapper.vm.$data.size).toBe("is-large");
      });
    });
    describe("On mobile device", () => {
      beforeEach(() => {
        const state = () => ({
          leagues: [],
          last_update: undefined,
          country: undefined,
          level: undefined,
          loading: false
        });
        store = new Vuex.Store({
          modules: {
            ranking: {
              namespaced: true,
              state,
              getters: ranking.getters
            }
          }
        });
        deviceService.isMobile.mockImplementation(() => true);
        wrapper = shallowMount(RankingTable, {
          localVue,
          store
        });
      });
      test("pagination is set to 5", () => {
        expect(wrapper.vm.$data.pagination).toBe(5);
      });
      test("size is set to empty string", () => {
        expect(wrapper.vm.$data.size).toBe("");
      });
    });
  });
  describe("#dlResults", () => {
    beforeEach(() => {
      const state = () => ({
        leagues: [{ name: "test" }],
        last_update: new Date().toISOString(),
        country: "Test Country",
        level: 6,
        loading: false
      });
      store = new Vuex.Store({
        modules: {
          ranking: {
            namespaced: true,
            state,
            getters: ranking.getters
          }
        }
      });
      wrapper = shallowMount(RankingTable, {
        localVue,
        store
      });
    });
    describe("When ranking service return data", () => {
      let click;
      let createElement;
      beforeEach(() => {
        click = jest.fn();
        createElement = jest
          .spyOn(document, "createElement")
          .mockReturnValueOnce({
            href: "https://test.com",
            download: "test.csv",
            click
          });
        global.URL.createObjectURL = jest.fn();
        global.URL.revokeObjectURL = jest.fn();
      });
      test("'a' element created", async () => {
        await wrapper.vm.dlResults();
        expect(createElement).toHaveBeenCalledWith("a");
      });
      test("click on 'a' element", async () => {
        await wrapper.vm.dlResults();
        expect(click).toHaveBeenCalled();
      });
      test("URL is build", async () => {
        await wrapper.vm.dlResults();
        expect(global.URL.createObjectURL).toHaveBeenCalled();
      });
      test("URL is revoked", async () => {
        await wrapper.vm.dlResults();
        expect(global.URL.revokeObjectURL).toHaveBeenCalled();
      });
    });
    describe("When ranking service reject query", () => {
      beforeEach(() => {
        const state = () => ({
          leagues: [{ name: "test" }],
          last_update: new Date().toISOString(),
          country: "Test Country",
          level: 6,
          loading: false
        });
        store = new Vuex.Store({
          modules: {
            ranking: {
              namespaced: true,
              state,
              getters: ranking.getters
            }
          }
        });
        rankingService.download.mockImplementation(() =>
          Promise.reject("test rejection")
        );
        wrapper = shallowMount(RankingTable, {
          localVue,
          store,
          mocks: {
            $buefy
          }
        });
      });
      test("Call buefy toaster", async () => {
        await wrapper.vm.dlResults();
        expect($buefy.toast.open).toHaveBeenLastCalledWith({
          message: "test rejection",
          type: "is-danger",
          duration: expect.any(Number)
        });
      });
    });
  });
});
