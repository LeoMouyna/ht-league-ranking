import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import RankingLeagueForm from "@/components/RankingLeagueForm.vue";
import VueRouter from "vue-router";
import Buefy from "buefy";
import Vuex from "vuex";
// import rankingService from "@/services/rankingService.js";

config.mocks.$t = key => key;

jest.mock("@/services/rankingService.js");

const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(Buefy);
localVue.use(Vuex);

let wrapper = shallowMount(RankingLeagueForm, {
  localVue
});

describe("RankingLeagueForm component", () => {
  test("mount correctly", () => {
    expect(wrapper).toBeTruthy();
  });
});
