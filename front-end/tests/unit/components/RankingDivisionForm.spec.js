import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import RankingDivisionForm from "@/components/RankingDivisionForm.vue";
import countryService from "@/services/countryService.js";
import VueRouter from "vue-router";
import Buefy from "buefy";
import Vuex from "vuex";

config.mocks.$t = key => key;
config.mocks.$tc = () => "tc mocked !";

jest.mock("@/services/countryService.js");

const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(Buefy);
localVue.use(Vuex);
const router = new VueRouter();

const $router = {
  push: jest.fn()
};

const $buefy = {
  toast: {
    open: jest.fn()
  }
};

let wrapper = shallowMount(RankingDivisionForm, {
  localVue,
  router
});

describe("RankingDivisionForm component", () => {
  describe("#mounted", () => {
    describe("When countryService return a valid list", () => {
      test("Handle country list", async () => {
        expect(wrapper.vm.$data.countries).toEqual(countryService.countries);
        expect(countryService.getAll).toHaveBeenCalled();
      });
    });
    describe("When countryService return an empty list", () => {
      beforeAll(() => {
        countryService.getAll
          .mockImplementation(() => {
            return Promise.resolve({
              data: []
            });
          })
          .mockName("Empty data");
        wrapper = shallowMount(RankingDivisionForm, {
          localVue,
          router
        });
      });
      test("Handle empty list", async () => {
        expect(countryService.getAll).toHaveBeenCalled();
        expect(wrapper.vm.$data.countries).toEqual([]);
      });
    });
    describe("When countryService return an error", () => {
      beforeAll(() => {
        countryService.getAll
          .mockImplementation(() => {
            return Promise.reject("test rejection");
          })
          .mockName("Promise rejection");
        wrapper = shallowMount(RankingDivisionForm, {
          localVue,
          mocks: {
            $buefy
          }
        });
      });
      test("Handle promise reject", async () => {
        expect(countryService.getAll).toHaveBeenCalled();
        expect(wrapper.vm.$data.countries).toEqual([]);
        expect(wrapper.vm.$buefy.toast.open).toHaveBeenCalledWith({
          message: "test rejection",
          type: "is-danger",
          duration: 5000
        });
      });
    });
  });
  describe("#filteredCountries", () => {
    beforeAll(() => {
      countryService.getAll
        .mockImplementation(() => {
          return Promise.resolve({
            data: countryService.countries
          });
        })
        .mockName("Default data");
      wrapper = shallowMount(RankingDivisionForm, {
        localVue,
        router
      });
    });
    describe("When country name input is empty", () => {
      test("Doesn't filter", () => {
        expect(wrapper.vm.filteredCountries).toEqual(
          wrapper.vm.$data.countries
        );
      });
    });
    describe("When country name input doesn't match any country", () => {
      test("Return empty list", () => {
        wrapper.vm.$data.countryName = "Zim";
        expect(wrapper.vm.filteredCountries).toEqual([]);
      });
    });
    describe("When country name match at least one country", () => {
      test("Return all match countries", () => {
        wrapper.vm.$data.countryName = "F";
        expect(wrapper.vm.filteredCountries).toEqual([
          countryService.countries[1],
          countryService.countries[2]
        ]);
      });
      test("Is case insensitve", () => {
        wrapper.vm.$data.countryName = "sWED";
        expect(wrapper.vm.filteredCountries).toEqual([
          countryService.countries[0]
        ]);
      });
    });
  });
  describe("#selectLeague", () => {
    let actions;
    let store;
    const localVueWithoutRouter = createLocalVue();
    localVueWithoutRouter.use(Vuex);
    localVueWithoutRouter.use(Buefy);
    beforeEach(() => {
      actions = {
        setDivision: jest.fn()
      };
      store = new Vuex.Store({
        modules: {
          division: {
            namespaced: true,
            actions
          }
        }
      });
      wrapper = shallowMount(RankingDivisionForm, {
        store,
        localVue: localVueWithoutRouter,
        mocks: {
          $router
        }
      });
      wrapper.vm.$data.country = countryService.countries[1]; // France
      wrapper.vm.$data.league = countryService.countries[1].leagues[1]; // 2nd Division
    });
    test("Call setDivision from state", async () => {
      await wrapper.vm.selectLeague();
      expect(actions.setDivision).toHaveBeenCalledWith(expect.any(Object), {
        country: "France",
        countryId: 2,
        level: 2,
        firstLeagueId: 7,
        leagueSize: 4
      });
    });
    test("Call router push", async () => {
      await wrapper.vm.selectLeague();
      expect($router.push).toHaveBeenCalledWith({
        name: "Results",
        query: { country: "France", level: 2 }
      });
    });
  });
});
