import { shallowMount, createLocalVue } from "@vue/test-utils";
import Auth from "@/components/Auth.vue";
import Buefy from "buefy";

const localVue = createLocalVue();
localVue.use(Buefy);

let wrapper;
describe("Auth.vue", () => {
  test("mount correctly", () => {
    wrapper = shallowMount(Auth, { localVue });
    expect(wrapper).toBeTruthy();
  });
});
