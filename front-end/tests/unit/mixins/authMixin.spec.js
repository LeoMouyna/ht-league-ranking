import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import Buefy from "buefy";
import Vuex from "vuex";
import { authViewMixin } from "@/mixins/authMixin";
import { buildStore } from "../helpers/store.js";
import authService from "@/services/authService.js";
import { authComponentMixin } from "../../../src/mixins/authMixin.js";

config.mocks.$t = key => key;
config.mocks.$tc = params => params;

jest.mock("@/services/authService.js");

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(Buefy);

let wrapper;
let store = buildStore(null, null, null, null, null, null, null, null);
let $buefy;

const component = {
  template: `<div></div>`
};

describe("authMixin", () => {
  let authActions;
  let authState;
  beforeEach(() => {
    store = buildStore(
      null,
      null,
      null,
      null,
      null,
      null,
      authActions,
      authState
    );
  });
  describe("shared functions", () => {
    beforeAll(() => {
      authActions = {
        set: jest.fn(),
        updateToken: jest.fn(),
        updateProfile: jest.fn(),
        logout: jest.fn()
      };
    });
    beforeEach(() => {
      $buefy = {
        toast: { open: jest.fn() }
      };
      wrapper = shallowMount(component, {
        localVue,
        store,
        mixins: [authViewMixin],
        mocks: {
          $buefy
        }
      });
    });
    test("mount correctly", () => {
      expect(wrapper).toBeTruthy();
    });
    describe("#syncUserData", () => {
      test("call fetchProfile from authService", async () => {
        authService.fetchProfile.mockClear();
        await wrapper.vm.syncUserData();
        expect(authService.fetchProfile).toHaveBeenCalledTimes(1);
      });
      describe("when fetchProfile return an OK response", () => {
        test("update store profile", async () => {
          await wrapper.vm.syncUserData();
          expect(authActions.updateProfile).toHaveBeenLastCalledWith(
            expect.any(Object),
            {
              name: "Whatever"
            }
          );
        });
      });
      describe("when fetchProfile return an Unauthorized response", () => {
        beforeEach(() => {
          authService.fetchProfile.mockRejectedValueOnce({
            response: {
              data: { message: "Unauthorized", code: "unauthorized" },
              status: 401
            }
          });
        });
        test("remove stored tokens", async () => {
          const removeAuthTokenSpy = jest.spyOn(wrapper.vm, "removeAuthToken");
          await wrapper.vm.syncUserData();
          expect(removeAuthTokenSpy).toHaveBeenCalledTimes(1);
        });
        test("display an error message", async () => {
          await wrapper.vm.syncUserData();
          expect($buefy.toast.open).toHaveBeenCalledWith({
            message: "unauthorized",
            type: "is-warning"
          });
        });
      });
      describe("when fetchProfile return an other error response", () => {
        beforeEach(() => {
          authService.fetchProfile.mockRejectedValueOnce({
            message: "test error"
          });
        });
        test("display an error message", async () => {
          await wrapper.vm.syncUserData();
          expect($buefy.toast.open).toHaveBeenCalledWith({
            message: "errors.somethingWentWrong",
            type: "is-danger"
          });
        });
      });
    });
    describe("#removeAuthToken", () => {
      test("remove token from authentication store", () => {
        authActions.logout.mockClear();
        wrapper.vm.removeAuthToken();
        expect(authActions.logout).toHaveBeenCalledTimes(1);
      });
      test("remove token from Cookies and localStorage", () => {
        authService.removeToken.mockClear();
        wrapper.vm.removeAuthToken();
        expect(authService.removeToken).toHaveBeenCalledTimes(1);
      });
    });
  });
  describe("authViewMixin", () => {
    describe("#beforeMount", () => {
      describe("when user is authenticated", () => {
        let syncUserDataSpy;
        beforeAll(() => {
          authState = () => ({
            token: "qwertyuiopasdfghjk"
          });
        });
        beforeEach(() => {
          wrapper = shallowMount(component, {
            localVue,
            store,
            mixins: [authViewMixin],
            mocks: {
              $buefy
            }
          });
          syncUserDataSpy = jest.spyOn(wrapper.vm, "syncUserData");
          wrapper.vm.$mount();
        });
        test("detect user as authenticated", () => {
          expect(wrapper.vm.userAuthenticated).toBeTruthy();
        });
        describe("when profile is not set yet", () => {
          beforeAll(() => {
            authState = () => ({
              token: "qwertyuiopasdfghjk",
              profile: undefined
            });
          });
          test("call syncUserData", () => {
            expect(syncUserDataSpy).toHaveBeenCalledTimes(1);
          });
        });
        describe("when profile is already set", () => {
          beforeAll(() => {
            authState = () => ({
              token: "qwertyuiopasdfghjk",
              profile: { name: "Whatever" }
            });
          });
          test("don't call syncUserdata", () => {
            expect(syncUserDataSpy).toHaveBeenCalledTimes(0);
          });
        });
      });
      describe("when user is not authenticated", () => {
        let syncUserDataSpy;
        beforeAll(() => {
          authState = () => ({
            serverAuthenticated: false,
            token: undefined,
            profile: undefined
          });
          authActions = {
            set: jest.fn(),
            updateToken: jest.fn(),
            updateProfile: jest.fn(),
            logout: jest.fn()
          };
        });
        beforeEach(() => {
          wrapper = shallowMount(component, {
            localVue,
            store,
            mixins: [authViewMixin],
            mocks: {
              $buefy
            }
          });
          authService.retrieveToken.mockClear();
          authService.findTokenInCookies.mockClear();
          syncUserDataSpy = jest.spyOn(wrapper.vm, "syncUserData");
          wrapper.vm.$mount();
        });
        test("detect user as not authenticated", () => {
          expect(wrapper.vm.userAuthenticated).toBeFalsy();
        });
        test("look for token in localStorage", () => {
          expect(authService.retrieveToken).toHaveBeenCalledTimes(1);
        });
        test("look for token in cookies", () => {
          expect(authService.findTokenInCookies).toHaveBeenCalledTimes(1);
        });
        describe("when token is stored in localStorage", () => {
          const localStorageToken = "localStorageToken";
          beforeEach(() => {
            authService.retrieveToken.mockReturnValueOnce(localStorageToken);
          });
          test("update store with received token", () => {
            wrapper.vm.$mount();
            expect(authActions.updateToken).toHaveBeenCalledWith(
              expect.any(Object),
              localStorageToken
            );
          });
          test("call syncUserData", () => {
            expect(syncUserDataSpy).toHaveBeenCalledTimes(1);
          });
        });
        describe("when token is stored as a Cookie", () => {
          const cookieToken = "cookieToken";
          beforeEach(() => {
            authService.retrieveToken.mockReturnValueOnce(undefined);
            authService.findTokenInCookies.mockReturnValueOnce(cookieToken);
            authService.storeToken.mockClear();
            syncUserDataSpy.mockClear();
            wrapper.vm.$mount();
          });
          test("update localStorage token with received token", () => {
            expect(authService.storeToken).toHaveBeenCalledWith(cookieToken);
          });
          test("update store with received token", () => {
            expect(authActions.updateToken).toHaveBeenCalledWith(
              expect.any(Object),
              cookieToken
            );
          });
          test("call syncUserData", () => {
            expect(syncUserDataSpy).toHaveBeenCalledTimes(1);
          });
        });
        describe("when no token has been provided yet", () => {
          beforeEach(() => {
            authService.retrieveToken.mockReturnValueOnce(undefined);
            authService.findTokenInCookies.mockReturnValueOnce(undefined);
            authService.storeToken.mockClear();
            syncUserDataSpy.mockClear();
            wrapper.vm.$mount();
          });
          test("don't call syncUserData", () => {
            expect(syncUserDataSpy).toHaveBeenCalledTimes(0);
          });
        });
      });
    });
  });
  describe("authComponentMixin", () => {
    beforeEach(() => {
      wrapper = shallowMount(component, {
        localVue,
        store,
        mixins: [authComponentMixin],
        mocks: {
          $buefy
        }
      });
    });
    describe("#login", () => {
      test("ask for a login", async () => {
        authService.login.mockClear();
        await wrapper.vm.login();
        expect(authService.login).toHaveBeenCalledTimes(1);
      });
      test("display a redirection message", async () => {
        await wrapper.vm.login();
        expect($buefy.toast.open).toHaveBeenCalledWith({
          message: "links.redirection <a href=https://example/com>hattrick</a>",
          type: "is-warning"
        });
      });
      describe("when login return an error", () => {
        beforeEach(() => {
          authService.login.mockRejectedValueOnce({ message: "test error" });
        });
        test("display an error message", async () => {
          await wrapper.vm.login();
          expect($buefy.toast.open).toHaveBeenCalledWith({
            message: "errors.somethingWentWrong",
            type: "is-danger"
          });
        });
      });
    });
    describe("#logout", () => {
      test("ask for a logout", async () => {
        authService.logout.mockClear();
        await wrapper.vm.logout();
        expect(authService.logout).toHaveBeenCalledTimes(1);
      });
      test("remove auth token", async () => {
        const removeAuthTokenSpy = jest.spyOn(wrapper.vm, "removeAuthToken");
        await wrapper.vm.logout();
        expect(removeAuthTokenSpy).toHaveBeenCalledTimes(1);
      });
      describe("when logout return an error", () => {
        beforeEach(() => {
          authService.logout.mockRejectedValueOnce({ message: "test error" });
        });
        test("display an error message", async () => {
          await wrapper.vm.logout();
          expect($buefy.toast.open).toHaveBeenCalledWith({
            message: "errors.somethingWentWrong",
            type: "is-danger"
          });
        });
        test("remove auth token any way", async () => {
          const removeAuthTokenSpy = jest.spyOn(wrapper.vm, "removeAuthToken");
          await wrapper.vm.logout();
          expect(removeAuthTokenSpy).toHaveBeenCalledTimes(1);
        });
      });
    });
  });
});
