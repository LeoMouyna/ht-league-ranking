import { shallowMount, createLocalVue, config } from "@vue/test-utils";
import Results from "@/views/Results.vue";
import Buefy from "buefy";
import Vuex from "vuex";
import countryService from "@/services/countryService.js";
import rankingService from "@/services/rankingService.js";
import { buildStore } from "../helpers/store.js";

config.mocks.$t = key => key;
config.mocks.$tc = params => params;

jest.mock("@/services/countryService.js");
jest.mock("@/services/rankingService.js");
jest.mock("@/services/deviceService.js");

const localVue = createLocalVue();
localVue.use(Buefy);
localVue.use(Vuex);

let $router;

let $buefy;

let store;

let wrapper;

describe("Results component", () => {
  const bothRequired = "errors.invalidRequiredDivisionForm";
  beforeEach(() => {
    $buefy = {
      toast: { open: jest.fn() }
    };
    $router = {
      push: jest.fn()
    };
    store = buildStore(null, null, null, null, null, null, null, null);
  });
  test("mount correctly", () => {
    wrapper = shallowMount(Results, {
      localVue,
      propsData: {
        countryName: "Test Country",
        divisionLevel: 6
      },
      store: store,
      mocks: {
        $router
      }
    });
    expect(wrapper).toBeTruthy();
    expect(wrapper.vm.$props.countryName).toBe("Test Country");
    expect(wrapper.vm.$props.divisionLevel).toBe(6);
  });
  describe("#beforeMount()", () => {
    const displayMessageAndRedirectMock = jest.spyOn(
      Results.methods,
      "displayMessageAndRedirect"
    );
    describe("When props are invalid", () => {
      describe("empty countryName", () => {
        beforeEach(() => {
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: undefined,
              divisionLevel: 6
            },
            store,
            mocks: {
              $router,
              $buefy
            }
          });
        });
        test("Call displayMessageAndRedirect", () => {
          expect(displayMessageAndRedirectMock).toHaveBeenCalledWith(
            bothRequired
          );
        });
      });
      describe("empty divisionLevel", () => {
        beforeEach(() => {
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: "France",
              divisionLevel: undefined
            },
            store,
            mocks: {
              $router,
              $buefy
            }
          });
        });
        test("Call displayMessageAndRedirect", () => {
          expect(displayMessageAndRedirectMock).toHaveBeenCalledWith(
            bothRequired
          );
        });
      });
      describe("string as divisionLevel", () => {
        beforeEach(() => {
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: "France",
              divisionLevel: "12"
            },
            store,
            mocks: {
              $router,
              $buefy
            }
          });
        });
        test("Call displayMessageAndRedirect", () => {
          expect(displayMessageAndRedirectMock).toHaveBeenCalledWith(
            bothRequired
          );
        });
      });
      describe("list as divisionLevel", () => {
        beforeEach(() => {
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: "France",
              divisionLevel: [12]
            },
            store,
            mocks: {
              $router,
              $buefy
            }
          });
        });
        test("Call displayMessageAndRedirect", () => {
          expect(displayMessageAndRedirectMock).toHaveBeenCalledWith(
            bothRequired
          );
        });
      });
      describe("number as countryName", () => {
        beforeEach(() => {
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: 123,
              divisionLevel: 6
            },
            store,
            mocks: {
              $router,
              $buefy
            }
          });
        });
        test("Call displayMessageAndRedirect", () => {
          expect(displayMessageAndRedirectMock).toHaveBeenCalledWith(
            bothRequired
          );
        });
      });
      describe("list as countryName", () => {
        beforeEach(() => {
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: ["France", "England"],
              divisionLevel: 6
            },
            store,
            mocks: {
              $router,
              $buefy
            }
          });
        });
        test("Call displayMessageAndRedirect", () => {
          expect(displayMessageAndRedirectMock).toHaveBeenCalledWith(
            bothRequired
          );
        });
      });
    });
    describe("When props are different from stored division", () => {
      describe("When countryName match more than one country", () => {
        beforeEach(() => {
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: "f",
              divisionLevel: 2
            },
            store,
            mocks: {
              $router,
              $buefy
            }
          });
        });
        test("Call getByName from countryService", () => {
          expect(countryService.getByName).toHaveBeenCalledWith("f");
        });
        test("Call displayMessageAndRedirect", () => {
          expect(displayMessageAndRedirectMock).toHaveBeenCalledWith(
            "errors.tooManyMatchingCountries"
          );
        });
      });
      describe("When countryName match to one country only", () => {
        let getOrComputedMock;
        let divisionActions;
        beforeEach(() => {
          divisionActions = {
            setDivision: jest.fn()
          };
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: "fra",
              divisionLevel: 2
            },
            store: buildStore(
              null,
              null,
              divisionActions,
              null,
              null,
              null,
              null,
              null
            ),
            mocks: {
              $router,
              $buefy
            }
          });
          getOrComputedMock = jest.spyOn(
            Results.methods,
            "getOrComputeRanking"
          );
        });
        test("Call getByName from countryService", () => {
          expect(countryService.getByName).toHaveBeenCalledWith("fra");
        });
        test("Call setDivision from division store", () => {
          const france = countryService.countries[1];
          expect(divisionActions.setDivision).toHaveBeenCalledWith(
            expect.any(Object),
            {
              country: france.name,
              countryId: france.ht_league_id,
              level: france.leagues[1].level,
              firstLeagueId: france.leagues[1].firstId,
              leagueSize: france.leagues[1].size
            }
          );
        });
        test("Call getOrComputeRanking", () => {
          expect(getOrComputedMock).toHaveBeenCalled();
        });
        describe("When division level doesn't exist", () => {
          beforeEach(() => {
            wrapper = shallowMount(Results, {
              localVue,
              propsData: {
                countryName: "fra",
                divisionLevel: 5
              },
              store,
              mocks: {
                $router,
                $buefy
              }
            });
          });
          test("Call displayMessageAndRedirect", () => {
            expect(displayMessageAndRedirectMock).toHaveBeenCalledWith(
              "errors.noMatchingDivisionForm"
            );
          });
        });
      });
    });
  });
  describe("#displayMessageAndRedirect()", () => {
    beforeEach(() => {
      beforeEach(() => {
        $buefy = {
          toast: { open: jest.fn() }
        };
        $router = {
          push: jest.fn()
        };
        wrapper = shallowMount(Results, {
          localVue,
          propsData: {
            countryName: "Fra",
            divisionLevel: 2
          },
          store,
          mocks: {
            $router,
            $buefy
          }
        });
      });
      describe("When message is set", () => {
        test("Display error toaster", () => {
          const message = "Bonjour";
          wrapper.vm.displayMessageAndRedirect(message);
          expect($buefy.toast.open).toHaveBeenCalledWith({
            message,
            type: "is-danger",
            duration: expect.any(Number)
          });
        });
        test("Return to home page by default", () => {
          expect($router.push).toHaveBeenCalledWith({ name: "Home" });
        });
      });
      describe("When pageName is set", () => {
        test("Return to specific page", () => {
          const name = "test";
          wrapper.vm.displayMessageAndRedirect("message", name);
          expect($router.push).toHaveBeenCalledWith({ name });
        });
      });
    });
  });
  describe("#getOrComputeRanking()", () => {
    let rankingActions;
    let rankingState;
    let divisionState;
    beforeEach(() => {
      rankingActions = {
        set: jest.fn(),
        toggleRanking: jest.fn()
      };
      rankingState = () => ({
        country: "France",
        countryId: 0,
        level: 2,
        firstLeagueId: 0,
        leagueSize: 4
      });
      divisionState = () => ({
        country: "France",
        countryId: 0,
        level: 2,
        firstLeagueId: 0,
        leagueSize: 4
      });
      store = buildStore(
        rankingActions,
        rankingState,
        null,
        divisionState,
        null,
        null,
        null,
        null
      );
    });
    describe("When rankingService return a 500", () => {
      beforeEach(async () => {
        rankingService.getDivision
          .mockImplementationOnce(() => {
            return Promise.reject({ response: { status: 500 } });
          })
          .mockName("response 500");
        $buefy = {
          toast: { open: jest.fn() }
        };
        wrapper = shallowMount(Results, {
          localVue,
          propsData: {
            countryName: "Fra",
            divisionLevel: 2
          },
          store,
          mocks: {
            $router,
            $buefy
          }
        });
      });
      test("Call toggleRanking from ranking store module", () => {
        expect(rankingActions.toggleRanking).toHaveBeenCalledTimes(2);
      });
      test("Call getDivision from ranking service", () => {
        expect(rankingService.getDivision).toHaveBeenCalledWith("France", 2);
      });
      test("Display an error message on a toaster", () => {
        expect($buefy.toast.open).toHaveBeenCalledWith({
          message: "errors.somethingWentWrong",
          type: "is-danger",
          duration: expect.any(Number)
        });
      });
    });
    describe("When rankingService return a 404", () => {
      const resMessage = "No division rigth now";
      let fetchDivision;
      beforeEach(() => {
        rankingService.getDivision
          .mockImplementationOnce(() => {
            return Promise.reject({
              response: { status: 404, data: { message: resMessage } }
            });
          })
          .mockName("response 404");
        fetchDivision = jest.fn();
        $buefy = {
          toast: { open: jest.fn() }
        };
        wrapper = shallowMount(Results, {
          localVue,
          propsData: {
            countryName: "Fra",
            divisionLevel: 2
          },
          store,
          mocks: {
            $router,
            $buefy
          },
          // FIXME: Will be deprecated we need to migrate fetchDivision into isolated file to mock it. Maybe a mixin
          methods: { ...Results.methods, fetchDivision }
        });
      });
      test("Call toggleRanking from ranking store module", () => {
        console.log("Check toggleRanking");
        expect(rankingActions.toggleRanking).toHaveBeenCalledTimes(2);
      });
      test("Call getDivision from ranking service", () => {
        expect(rankingService.getDivision).toHaveBeenCalledWith("France", 2);
      });
      test("Display an warning message on a toaster", () => {
        expect($buefy.toast.open).toHaveBeenCalledWith({
          message: "errors.fetchingHattrickData",
          type: "is-warning",
          duration: expect.any(Number)
        });
      });
      test("Call fetchDivision method", () => {
        expect(fetchDivision).toHaveBeenCalled();
      });
    });
    describe("When rankingService return a division", () => {
      beforeEach(() => {
        $buefy = {
          toast: { open: jest.fn() }
        };
        wrapper = shallowMount(Results, {
          localVue,
          propsData: {
            countryName: "Fra",
            divisionLevel: 2
          },
          store,
          mocks: {
            $router,
            $buefy
          }
        });
      });
      test("Call toggleRanking from ranking store module", () => {
        expect(rankingActions.toggleRanking).toHaveBeenCalledTimes(2);
      });
      test("Call getDivision from ranking service", () => {
        expect(rankingService.getDivision).toHaveBeenCalledWith("France", 2);
      });
      test("Call set from ranking store module", () => {
        expect(rankingActions.set).toHaveBeenCalledWith(expect.any(Object), {
          leagues: expect.any(Array),
          country: "France",
          level: 2,
          last_update: expect.any(String)
        });
      });
      test("Display a message toaster", () => {
        expect($buefy.toast.open).toHaveBeenCalledWith({
          message: "success.addLeaguesFromDatabase",
          type: "is-success",
          duration: expect.any(Number)
        });
      });
    });
  });
  describe("#fetchDivision()", () => {
    let rankingActions;
    let rankingState;
    let divisionState;
    let taskActions;
    beforeEach(() => {
      rankingActions = {
        set: jest.fn(),
        toggleRanking: jest.fn()
      };
      rankingState = () => ({
        country: "France",
        countryId: 0,
        level: 2,
        firstLeagueId: 0,
        leagueSize: 4
      });
      divisionState = () => ({
        country: "France",
        countryId: 0,
        level: 2,
        firstLeagueId: 0,
        leagueSize: 4
      });
      taskActions = {
        updateRunningTask: jest.fn(),
        setTask: jest.fn()
      };
      store = buildStore(
        rankingActions,
        rankingState,
        null,
        divisionState,
        taskActions,
        null,
        null,
        null
      );
    });
    describe("When user is not authenticated", () => {
      test("Doesn't toggleRanking", async () => {
        rankingActions.toggleRanking.mockClear();
        await wrapper.vm.fetchDivision();
        expect(rankingActions.toggleRanking).toHaveBeenCalledTimes(0);
      });
      test("Doesn't updateRunningTask", () => {
        expect(taskActions.updateRunningTask).toHaveBeenCalledTimes(0);
      });
      test("Doesn't ask for taskEvent from task store module", () => {
        expect(rankingService.getTaskEvent).toHaveBeenCalledTimes(0);
      });
    });
    describe("When user is authenticated", () => {
      beforeEach(() => {
        const authState = () => ({
          serverAuthenticated: true,
          token: "qwertyuiopasdfghjklzxcvbnm",
          profile: { name: "whatever" }
        });
        store = buildStore(
          rankingActions,
          rankingState,
          null,
          divisionState,
          taskActions,
          null,
          null,
          authState
        );
      });
      describe("When rankingService forceDivision return an error", () => {
        beforeEach(async () => {
          rankingService.forceDivision
            .mockRejectedValue({ response: { status: 500 } })
            .mockName("response 500");
          $buefy = {
            toast: { open: jest.fn() }
          };
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: "Fra",
              divisionLevel: 2
            },
            store,
            mocks: {
              $router,
              $buefy
            }
          });
          taskActions.updateRunningTask.mockClear();
          rankingService.getTaskEvent.mockClear();
          await wrapper.vm.fetchDivision();
        });
        test("Call toggleRanking from ranking store module", async () => {
          rankingActions.toggleRanking.mockClear();
          await wrapper.vm.fetchDivision();
          expect(rankingActions.toggleRanking).toHaveBeenCalledTimes(2);
        });
        test("Call updateRunningTask from task store module to set initial state", () => {
          expect(taskActions.updateRunningTask).toHaveBeenNthCalledWith(
            1,
            expect.any(Object),
            {
              ratio: 0,
              status: "running"
            }
          );
        });
        test("Call updateRunningTask from task store module to set error state", () => {
          expect(taskActions.updateRunningTask).toHaveBeenNthCalledWith(
            2,
            expect.any(Object),
            {
              ratio: undefined,
              status: "error"
            }
          );
        });
        test("Doesn't ask for taskEvent from task store module", () => {
          expect(rankingService.getTaskEvent).toHaveBeenCalledTimes(0);
        });
        test("Display an error message on a toaster", () => {
          expect($buefy.toast.open).toHaveBeenCalledWith({
            message: "errors.somethingWentWrong",
            type: "is-danger",
            duration: expect.any(Number)
          });
        });
        describe("When rankingService return a message", () => {
          beforeEach(() => {
            $buefy.toast.open.mockClear();
            rankingService.forceDivision
              .mockRejectedValue({
                response: {
                  status: 404,
                  data: { message: "url not allocated" }
                }
              })
              .mockName("response 404");
            wrapper.vm.fetchDivision();
          });
          test("response 404 mock has been called", () => {
            expect(rankingService.forceDivision.getMockName()).toBe(
              "response 404"
            );
          });
          test("Display returned error message on a toaster", () => {
            expect($buefy.toast.open).toHaveBeenCalledWith({
              message: "url not allocated",
              type: "is-danger",
              duration: expect.any(Number)
            });
          });
        });
      });
      describe("When rankingService forceDivision return a task", () => {
        const task = {
          task_id: 123456,
          task_status: "pending",
          country: "France",
          level: 2,
          start_date: new Date().toISOString()
        };
        beforeEach(async () => {
          rankingService.forceDivision
            .mockResolvedValue({
              status: 201,
              data: task
            })
            .mockName("response 201");
          $buefy = {
            toast: { open: jest.fn() }
          };
          wrapper = shallowMount(Results, {
            localVue,
            propsData: {
              countryName: "Fra",
              divisionLevel: 2
            },
            store,
            mocks: {
              $router,
              $buefy
            }
          });
          taskActions.updateRunningTask.mockClear();
          await wrapper.vm.fetchDivision();
        });
        test("response 201 mock has been called", () => {
          expect(rankingService.forceDivision.getMockName()).toBe(
            "response 201"
          );
        });
        test("Call toggleRanking from ranking store module", async () => {
          rankingActions.toggleRanking.mockClear();
          await wrapper.vm.fetchDivision();
          expect(rankingActions.toggleRanking).toHaveBeenCalledTimes(1);
        });
        test("Call updateRunningTask from task store module to set initial state", () => {
          expect(taskActions.updateRunningTask).toHaveBeenNthCalledWith(
            1,
            expect.any(Object),
            {
              ratio: 0,
              status: "running"
            }
          );
        });
        test("Call setTask from task store module to set starting task", () => {
          expect(taskActions.setTask).toHaveBeenCalledWith(
            expect.any(Object),
            task
          );
        });
        test("Display message toaster", () => {
          expect($buefy.toast.open).toHaveBeenCalledWith({
            message: "success.backgroundTask",
            type: "is-success",
            duration: expect.any(Number)
          });
        });
      });
    });
  });
});
