import { shallowMount, createLocalVue } from "@vue/test-utils";
import Home from "@/views/Home.vue";
import Buefy from "buefy";

const localVue = createLocalVue();
localVue.use(Buefy);

let wrapper;
describe("Home.vue", () => {
  test("mount correctly", () => {
    wrapper = shallowMount(Home, { localVue });
    expect(wrapper).toBeTruthy();
  });
});
